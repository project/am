/**
 * Global Variables
 */
var am = new Array();
var am_tinymce = false;
am['filter'] = new Array();

if (Drupal.jsEnabled) {
  $(document).ready(function(){
    $('#node-form').ajaxForm({ 
      url: '?q=am/save',
      dataType: 'json',
      success: function(data, status){
        $('#am-error').remove();
        if (data.status) {
          am['nodes'] = new Array(data.node);
          am['node'] = am['nodes'][0];
          updateList(am['nodes']);
          previewNode(am['nodes'][0]);
          $('#am-add-node').addClass('hide');
          $('#am-existing').removeClass('hide');
          $('#am-tabs li').removeClass('on');
          $('#am-tabs-existing').addClass('on');
          $('body').addClass('body-search');
          $('#node-form').resetForm();
        }
        else {
          msgdiv = document.createElement('div');
          $(msgdiv).attr('id', 'am-error');
          $(msgdiv).append(data.message);
          $('#node-form').before(msgdiv);				
        }
      }
    });
    
    $('#am-tabs-add-node a').click(function(cur) {
      swaptabs('add-node', 'existing');
      return false;
    });
    
    $('#am-tabs-existing a').click(function(cur) {
      swaptabs('existing', 'add-node');
      return false;
    }); 
  
    am['filter']['type'] = $('#node-type-filter option:selected').val();
    am['filter']['tid'] = $('#node-channel-filter option:selected').val();
  
    /* IE and Safari don't pick up the .click so we have to use .change on the parent element
     * and then drill down into the options and find [@selected]
     */
    if ($.browser.msie || $.browser.safari) {
      $('#node-channel-filter').change(function() {
        updateNodeTid($(this).children("[@selected]"));
      });
      $('#node-results').change(function() {
        showNodePreview($(this).children("[@selected]"));
      });
    }
    else {
      $('#node-channel-filter option').click(function() {
        updateNodeTid(this);
      });
      $('#node-results').click(function() {
        showNodePreview(this);
      });
    }
    refreshList();
    $('#node-name-filter').keyup(updateNodeFilter);
    $('#am-select-form').submit(selectNode);
  
    am['filter']['type'] = $('#node-type-filter option:selected').val();
    am['filter']['tid'] = $('#node-channel-filter option:selected').val();
  
  });
} 

function swaptabs(on, off) {
  $('#am-' + off).addClass('hide');
  $('#am-' + on).removeClass('hide');      
  $('#am-tabs li').removeClass('on');
  $('#am-tabs-' + on).addClass('on');
  if (on == 'add-node') {
    $('body').removeClass('body-search');
  }
  else {
    $('body').addClass('body-search');
  }
}

function updateNodeTid(select) {
  am['filter']['tid'] = $(select).val();
  refreshList();
}

function updateNodeType(select) {
  am['filter']['type'] = $(select).val(); 
  refreshList();
}

function updateNodeFilter() {
  am['filter']['name'] = $(this).val();
  refreshList();
}

function refreshList() { 
  am['url'] = '?q=/am/list/' + am['filter']['type'] + '/' + am['filter']['tid'];
  
  $.getJSON(am['url'],function(obj){
    if (obj.length) {
      am['nodes'] = filterList(new Array(obj.nodes));
    }
    else {
      am['nodes'] = filterList(obj.nodes);
    }
    updateList(am['nodes']);
  });  
}

function updateList(nodes) {
  var results = '';
  if (!nodes || nodes.length == 0) {
    results += 'No Results';
    $("#am-results").html(results);
  }
  else {
    for (var i=0;i < nodes.length;i++) {
      results += '<a href="#" onclick="showNodePreview(' + nodes[i].nid + '); return false" id="node-' + nodes[i].nid + '">' + nodes[i].thumb;
      results += '<span>' + nodes[i].title + '<i>, ' + nodes[i].type + '</i></span><br class="clear" /></a>';
    }
    $("#am-results").html(results);
    showNodePreview(nodes[0].nid);
  }
  

}

function filterList(nodes) {
  var output = new Array()
  if (!am['filter']['name']) {
    return nodes;
  }
  for (var i=0;i < nodes.length;i++) {
    var nodeTitle = nodes[i].title.toLowerCase();
    if (am['filter']['name']) {
      if (nodeTitle.substr(0,am['filter']['name'].length) == am['filter']['name'].toLowerCase()) {
        output.push(nodes[i]);
      }
    }
    else {
      return nodes;
    }
  }
  return output;
}

function showNodePreview(nid) {
  if (am['nodes']) {
    for (var i=0;i< am['nodes'].length;i++) {
      if (am['nodes'][i].nid == nid) {
        previewNode(am['nodes'][i]);
        /* !! TODO !!
         * This is sort of an odd place for the following 2 lines since it has nothing to do with the previewing 
         * of the node, will put it here for now, and refactor the onclick later.
         */
        $('#am-results > a').removeClass('on');
        $('#node-' + nid).addClass('on');
        am['node'] = am['nodes'][i];
      }
    }
  }
}

function previewNode(node) {
  $('#am-preview p').html('<a href="#" onclick="selectNode(); return false;">' + node.preview + '<b>Select this image</b></a><div style="display:none">' + node.node_preview + '</div>');
}

function selectNode() {
  if (!am['node']) {
    alert('You must select an asset from the left hand controls in order to continue.');
    return false;
  }
	if (!am_tinymce) {
	  amUpdateTarget(am['node']);
	}
	else {
		amUpdateTinyMCE(am['node'].nid);
	}
  return false;
}

function amUpdateTarget(node) {
	var update = node.nid;
  if (target) {
    $('#edit-' + window.opener.target_name[target]  + '-nid',window.opener.document).val(update);
    $('#' + window.opener.target_name[target]  + '-preview', window.opener.document).html(node.node_preview);
  }
  else {
    $('.am-target',window.opener.document).val(update);
    $('.am-preview', window.opener.document).html(node.node_preview);
  }
	window.close();	
}

function amUpdateTinyMCE(nid) {
	$('#asset-popup').html('please wait');
	insertToEditor(am['node']['content']);
	window.close();
}
