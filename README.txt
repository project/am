*******************************************************************************

Asset Manager (AM)

Description:
-------------------------------------------------------------------------------

  The Asset Manager is a module to allow administrators to manage CCK 
nodereference assets via taxonomy.  There are two parts to AM, a tinymce plugin
and a popup for assigning node reference assets to nodes on the fly.


History:
-------------------------------------------------------------------------------

  The driving force behind this module was to be able to create a custom CCK
node type called an "Image" to replace the Image module. We really like the 
imagefield module and imagecache and use these two modules extensively.  The
problem was that there was no clean "Image Assist" type module to be able to
manage our assets, nor was there a way to keep a hierarchy of previously
used images.  

  We created this module to be able to create a directory like structure via
Taxonomy, and then as you add images, assign them to this Taxonomy to allow
for re-use of these assets.  Upon initial creation, we decided that it would
be great to extend it and be able to manage _any_ type of CCK node reference.

  It only supports Images right now (out of the box) but can easily be extended
to support other types as well.


Installation:
-------------------------------------------------------------------------------
1.  Enable module in module list located at administer > build > modules.
2.  A new taxonomy is created for you called "Assets". This defines your directory structure.
3.  Add an "Asset Manager" field (Asset Field > Asset Manager Field) to any node type
4.  Use Asset Manager, conquer world.

Notes: 
The taxonomy used can be changed at admin/settings/am, and the "Assets" taxonomy can be modified to your needs.
Correct installation of jquery_update is also required, so ensure jquery.js has been updated.
Un-installing assetfield and asset manager and then re-installing them can have some weird behavior as everything
 is not 100% cleaned up. If you need to do this, you will need to manually fix taxonomies and fields for now.

  To use this module, you can add it as a tinymce plugin as well as a
nodereference field (assetfield).

TinyMCE Installation:
-------------------------------------------------------------------------------
1. Copy the drupalam subdirectory to the plugins subdirectory of the tinymce module.
2. Add the following 3 lines to tinymce's plugin_reg.php file (before return $plugins)
$plugins['drupalam'] = array();
$plugins['drupalam']['theme_advanced_buttons1'] = array('drupalam');
$plugins['drupalam']['extended_valid_elements'] = array('img[class|src|border=0|alt|title|width|height|align|name]');


Sponsor:
-------------------------------------------------------------------------------

This module was sponsored by Achieve Internet.


Authors:
-------------------------------------------------------------------------------

Steve Rude <steve@achieveinternet.com>
http://drupal.org/user/73183

Bill O'Connor <bill@achieveinternet.com>
http://drupal.org/user/63155

Chris Fuller <chris@achieveinternet.com>
http://drupal.org/user/61928

TODO
-------------------------------------------------------------------------------
+ create preview override system to make difficult content types easier, or
  abstract ourselves even further and ship with a few node type previews,
  e.g. image, video, audio that could be "dropped in"

+ create ajax form for switching between node types for adding

+ more admin controls for # of results, etc

+ refactor to remove any hard coded numbers or settings

