<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
  <title><?php print $title ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <meta http-equiv="imagetoolbar" content="no">
  <link rel="stylesheet" type="text/css" href="<?php print base_path() . drupal_get_path('module', 'am') ?>/am_window.css" />
  <?php print $head ?>
  <?php print $scripts ?>
  <style>
    body { margin: 0; }
  </style>
</head>

<body class="body-search">
<div id="am-header">
  <h1><?php print $title ?></h1>
  <ul id="am-tabs">
	<li id="am-tabs-existing" class="on"><a href="#"><span>Existing Assets</span></a></li>
	<li id="am-tabs-add-node"><a href="#"><span>Add New Asset <b>+</b></span></a></li>
  </ul>
</div><!-- END: am_header -->

<div id="am-existing">
  <div id="am-search">
    <h2>Filter:</h2>
    <?php print $filter ?>

    <h2>Results:</h2>
    <div id="am-results">
	  No Results
    </div><!-- END: am_results -->
  </div><!-- END: am_search -->

  <div id="am-preview">
    <p></p>
    <?php print $select ?>
  </div><!-- END: am_preview -->
</div><!-- END: am_existing -->

<div id="am-add-node" class="hide">
  <?php print $node_add ?>
</div>
</body>
</html>

