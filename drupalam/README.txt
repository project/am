
drupalam plugin for TinyMCE
------------------------------
Original plugin by benshell (image_assist)

Retooled for AM by Bill O'Connor (csevb10)

About:

   This plugin integrates the Drupal AM module with TinyMCE allowing
   you to upload, browse and insert content into your post. Please read the
   am documentation for details.

