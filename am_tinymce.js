/*
 * Note: 
 * This javascript file allows am to work with TinyMCE via the drupalimage plugin
 * for TinyMCE.  This file will be used instead of am_textarea.js when AM is called
 * from the drupalimage plugin.  Additional JS files similar to am_textarea.js and am_tinymce.js
 * could be created for using am with other WYSIWYG editors.
 */

// This doesn't work right.  The tiny_mce_popup.js file needs to be loaded BEFORE any setWindowArg() commands are issued
//document.write('<sc'+'ript language="javascript" type="text/javascript" src="' + BASE_URL + 'modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce_popup.js"><\/script>'); 

// get variables that were passed to this window from the tinyMCE editor
var nid;
var captionTitle;
var captionDesc;
var link;
var align;
var width;
var height;

function preinit() {
  // Initialize
  tinyMCE.setWindowArg('mce_windowresize', true);
  tinyMCE.setWindowArg('mce_replacevariables', false);
}

function initLoader() {

}

function initProperties() {

}

function initThumbs() {

}

function initHeader() {
}

function initUpload() {

}

function getFilterTag(formObj) {

}

function insertToEditor(content) {
  // Insert the image
  tinyMCE.execCommand("mceInsertContent", true, content);
  tinyMCE.selectedInstance.repaint();
  
  // Close the dialog
  tinyMCEPopup.close();
  return false;
}

function cancelAction() {
  // Close the dialog
  tinyMCEPopup.close();
}

// While loading
preinit();
