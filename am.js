/*
 * Asset Manager - Opens Asset Manager
 */
var target_name = new Array();
$(document).ready(function() {
  $('.am-loader').click(function(){
    var am_width=750;
    var am_height=585;
    if ($.browser.msie) {
      am_height=585; // IE6/7 renders slightly different than FF2 so we make it a little bit bigger
    }
    var target = target_name.length;
    target_name[target] = $(this).attr('id');
    target_name[target] = target_name[target].substr(0, target_name[target].length - 5);
    window.open(this.href + '?target=' + target, '_blank', 'width='+am_width+',height='+am_height+'status=0,toolbar=0,location=0,menubar=0,resizable=1,scrollbars=1')    
    $('#' + target_name[target] + '-preview').addClass('am-preview');
    $('#edit-' + target_name[target] + '-nid').addClass('am-target');
    return false;
  }); //ENDS AM_LOADER.CLICK
}); //ENDS READY()